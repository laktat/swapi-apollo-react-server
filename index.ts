'use strict';
const express = require('express');
const graphqlHTTP = require('express-graphql');
const cors = require('cors')

const PORT = process.env.PORT || 3001;
const server = express();

import { schema } from './src/graphql';

server.use( cors() )
server.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true,
}));

server.listen(PORT, () => {
  console.log(`Listening on http://localhost:${PORT}`);
});